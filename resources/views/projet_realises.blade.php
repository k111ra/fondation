@extends('layouts.layout-page')

@section('containt')

<section style="padding-top:100px; background-color:#f1efec;">
    <div class="container">

        <div class="row">
            <aside class="col-md-4 blog-sidebar">

                <div class="sidebar-box ftco-animate fadeInUp ftco-animated">
                    <h3 class="heading-2">Information Générale</h3>
                    <ul class="categories">
                        <ul class="categories">
                            <li class="active"><a href="/projet_realises">Nos Projets Réalisés</a></li>
                            <li ><a href="/#">Nos Projets Encours </a></li>
                            <li><a href="/#">Galeries</a></li>                        
                        </ul>
                    </ul>
                </div>

                {{-- <div class="p-4">
                    <h4 class="">Ressource et Media</h4>
                    <ol class="list-unstyled">
                        <li><a href="#">Lettre Annuel</a></li>
                        <li><a href="#">Rapport Annuel </a></li>
                        <li><a href="#">Presse</a></li>
                    </ol>
                </div> --}}
            </aside><!-- /.blog-sidebar -->
            <style>
                /* Style the buttons */
                .btn {
                    border: none;
                    outline: none;
                    padding: 10px 16px;
                    background-color: #f1f1f1;
                    cursor: pointer;
                }

                /* Style the active class (and buttons on mouse-over) */
                .active,
                .btn:hover {
                    background-color: #666;
                    color: white;
                }
            </style>

            @foreach ($pageProjetRealises as $pageProjetRealise)
            <div class="col-md-8 blog-main">
                <h2 style="font-family: Georgia,serif; font-size:13px; line-height:15px; font-style: italic; color:black; text-align: center;">
                    <span class="line" style="width: 30px; height:1px; background-color:black; display:inline-block; margin: 0 5px 3px 5px"></span>
                    {{ $pageProjetRealise->title }}
                    <span class="line" style="width: 30px; height:1px; background-color:black; display:inline-block; margin: 0 5px 3px 5px"></span>
                </h2>
                <h3 class="pb-4 mb-4 ">
                    {{ $pageProjetRealise->excerpt }}
                    <hr style="border-top: 3px solid #bbb;">
                </h3>
                <p> {!! $pageProjetRealise->body !!} </p>
                        <hr>
                @foreach ($projetRealises as $projetRealise)
                    
                <div class="blog-post">
                    <div class="container px-lg-5">
                        
                        <div class="row mx-lg-n5">
                            <div class="col py-3 px-lg-5 ">
                                <p> {!! $projetRealise->body !!}
                                </p>
                            </div>
                            <div class="col py-3 bg-light"><img src="storage/{{ $projetRealise->image }}" alt="" width="400px"></div>
                        </div>
                    </div>
                  
                </div><!-- /.blog-post -->
                <hr>
                @endforeach
                
               


            </div><!-- /.blog-main -->
            @endforeach
            



        </div>

    </div>
</section>

@endsection