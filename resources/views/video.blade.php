@extends('layouts.layout-page')

@section('containt')
<section class="py-5 text-center" style="background-image:url('/images/22989.jpg')">
    <div class="row py-lg-5">
      <div class="col-lg-6 col-md-8 m-auto">
        <h1 class="fw-light">Videos</h1>
        <p class="lead text-light"> Les Videos de La Fondation Kouyo</p>
        {{-- <p>
          <a href="#" class="btn btn-primary my-2">Main call to action</a>
          <a href="#" class="btn btn-secondary my-2">Secondary action</a>
        </p> --}}
      </div>
    </div>
  </section>
<section class="ftco-section ftco-degree-bg">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 ftco-animate">
                <div class="row">

                </div>
                <div class="row">
                    <div class="col-md-12 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch d-md-flex">
                            <video width="320" height="240" controls="true">
                                <source src="videos/PROJET-PODIA.mp4" type="video/mp4">
                                {{-- <source src="movie.ogg" type="video/ogg"> --}}
                              Your browser does not support the video tag.
                              </video>
                            <div class="text d-block pl-md-4">
                                <div class="meta mb-3">
                                    <div><a href="/actualites"></a></div>
                                </div>
                                <h3 class="heading"><a href="/actualites">PROJET PÔLE D'INTÉGRATION AGROPASTORALE (PODIA) AVEC LE RAFAA.</a></h3>
                                <p>27 janvier 2022 - Visite du site à Sikensi pour la mise en œuvre du PODIA (Acte 1) en partenariat avec le Ministère de la Femme, de la Famille et de l'Enfant pour le compte de la femme avec le RAFAA.</p>
                                <p><a href="/actualites" class="btn btn-primary py-2 px-3">Voir plus</a></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch d-md-flex">
                            <video width="320" height="240" controls="true">
                                <source src="videos/donfondation.mp4" type="video/mp4">
                                {{-- <source src="movie.ogg" type="video/ogg"> --}}
                              Your browser does not support the video tag.
                              </video>
                            <div class="text d-block pl-md-4">
                                <div class="meta mb-3">
                                    <div><a href="#"></a></div>
                                </div>
                                <h3 class="heading"><a href="#">Don de la Fondation KOUYO et du ministère de la femme et de l'enfant aux femmes veuve</a></h3>
                                <p>Don de la Fondation KOUYO et du ministère de la femme et de l'enfant aux femmes veuve</p>
                                <p><a href="#" class="btn btn-primary py-2 px-3">Voir plus</a></p>
                            </div>
                        </div>
                    </div>



                </div>
                <div class="row mt-5">
                    <div class="col">
                        <div class="block-27">

                            <ul>
                                {{-- <li><a href="#">&lt;</a></li>
                                <li class="active"><span>1</span></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">&gt;</a></li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div> <!-- .col-md-8 -->
            <div class="col-lg-4 sidebar ftco-animate">

                <div class="sidebar-box ftco-animate">
                    <h3 class="heading-2">Catégories</h3>
                    <ul class="categories">
                        <li><a href="#">Don <span>(12)</span></a></li>
                        <li><a href="#">Conferences <span>(22)</span></a></li>
                        <li><a href="#">Diner <span>(37)</span></a></li>
                        <li><a href="#">Gala <span>(42)</span></a></li>

                    </ul>
                </div>

                <div class="sidebar-box ftco-animate">
                    <h3 class="heading-2">Actualités récentes</h3>
                    <div class="block-21 mb-4 d-flex">
                        <a class="blog-img mr-4" style="background-image: url(images/podia.png);"></a>
                        <div class="text">
                            <h4 class="" style="font-size:14px"><a href="/video">PROJET PÔLE D'INTÉGRATION AGROPASTORALE (PODIA) AVEC LE RAFAA</a></h4>
                            <div class="meta">
                                <div><a href="#"><span class="icon-calendar"></span> Février 13, 2022</a></div>
                                <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                            </div>
                        </div>
                    </div>

                </div>




            </div>

        </div>
    </div>
</section> <!-- .section -->
@endsection
