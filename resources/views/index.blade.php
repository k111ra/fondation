@extends('layouts.layout')

@section('containt')

<script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.js"></script>

<style type="text/css">
    #overlay {
        position: relative;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: #000;
        filter: alpha(opacity=70);
        -moz-opacity: 0.7;
        -khtml-opacity: 0.7;
        opacity: 0.7;
        z-index: 1000000000;
        display: none;
    }

    .cnt223 a {
        text-decoration: none;
    }

    .popup {
        width: 100%;
        margin: 0 auto;
        display: none;
        position: relative;
        z-index: 1000000001;
    }

    .cnt223 {

        min-width: 200px;
        width: 400px;
        min-height: 150px;
        margin-top: 500px;
        background: green;
        position: fixed;
        z-index: 103;
        padding: 15px 35px;
        border-radius: 5px;
        box-shadow: 0 2px 5px #000;
    }

    .cnt223 p {
        clear: both;
        color: #555555;
        /* text-align: justify; */
        font-size: 14px;
        font-family: sans-serif;
    }

    .cnt223 p a {
        color: whitesmoke;
        font-weight: bold;
    }

    .cnt223 .x {
        float: right;
        height: 35px;
        left: 22px;
        position: fixed;
        top: -25px;
        width: 34px;
    }

    .cnt223 .x:hover {
        cursor: pointer;
    }
</style>
<script type='text/javascript'>
    $(function () {
        var overlay = $('<div id="overlay"></div>');
        overlay.show();
        overlay.appendTo(document.body);
        $('.popup').show();
        $('.close').click(function () {
            $('.popup').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });




        $('.x').click(function () {
            $('.popup').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });
    });
</script>
{{-- @foreach ($pensee as $pensee)
<div class='popup'>
    <div class='cnt223'>
        <a href='' class='close'>X</a>
        <h5 style="color:honeydew;">Pensée du jour:</h5>
        <p style="color: honeydew;">

            {!! $pensee->body !!}

        </p>
    </div>
</div>
@endforeach --}}

<section class="">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">

            @foreach ($slides as $slide)
            <div class="carousel-item {{ $slide->active }}">
                <img class="slide d-block w-100 slider " src="/storage/{{ $slide->image }}" alt="{{ $slide->titre }}">

                <div class="carousel-caption  d-md-block">
                    <div class="row">
                        <style>
                            blink {
                              animation: blinker 0.6s linear infinite;
                              color: #1c87c9;
                             }
                            @keyframes blinker {
                              50% { opacity: 0; }
                             }
                             .blink-one {
                               animation: blinker-one 1s linear infinite;
                             }
                             @keyframes blinker-one {
                               0% { opacity: 0; }
                             }
                             .blink-two {
                               animation: blinker-two 1.4s linear infinite;
                             }
                             @keyframes blinker-two {
                               100% { opacity: 0; }
                             }
                          </style>
                        <p style="display: inline-block; transform: rotate(-90deg); ">
                            <a onclick="document.getElementById('id01').style.display='block'"
                                class="w3-button btn-primary" style="background-color:orange;"><i
                                    class="ion-ios-play "></i></span> <span class="play"
                                    style="color: green; "> <blink>Video</blinl> </span></a>
                        </p>

                        <div class="col text-right ">
                            <h1 class=" font-weight-bold slideTest" style="color: green;" > {{ $slide->text }}</h1>
                            <p class="btn btn-primary py-3 px-4 slideTest" ><a href="/contact" >Rejoignez-nous
                                </a>
                            </p>
                        </div>

                    </div>


                </div>
            </div>
            @endforeach


        </div>

        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>


        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>





{{-- modal video --}}
<div id="id01" class="w3-modal" style="z-index: 100000">
    <div class="w3-modal-content">
        <div class="w3-container">
            {{-- <span onclick="document.getElementById('id01').style.display='none'"
                class="w3-button w3-display-topright">&times;</span>
            <iframe width="860" height="515" src="https://www.youtube.com/embed/o7kYmYV__Ec" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe> --}}
                <span onclick="document.getElementById('id01').style.display='none'"
                class="w3-button w3-display-topright">&times;</span>
                <div class="col-md-12 d-flex ftco-animate">
                    <div class="blog-entry align-self-stretch d-md-flex">
                        <video width="320" height="240" controls="true">
                            <source src="videos/PROJET-PODIA.mp4" type="video/mp4">
                            {{-- <source src="movie.ogg" type="video/ogg"> --}}
                          Your browser does not support the video tag.
                          </video>
                        <div class="text d-block pl-md-4">
                            <div class="meta mb-3">
                                <div><a href="/video"></a></div>
                            </div>
                            <h3 class="heading"><a href="/video">PROJET PÔLE D'INTÉGRATION AGROPASTORALE (PODIA) AVEC LE RAFAA.</a></h3>
                            <p>27 janvier 2022 - Visite du site à Sikensi pour la mise en œuvre du PODIA (Acte 1) en partenariat avec le Ministère de la Femme, de la Famille et de l'Enfant pour le compte de la femme avec le RAFAA.</p>
                            <p><a href="/video" class="btn btn-primary py-2 px-3">Voir plus</a></p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<section class="ftco-section ftco-no-pt ftco-no-pb ftco-volunteer">
    <div class="container" style="margin-top: 50px;">
        <div class="row" style="margin-bottom: 20px;">
            <div class="col-md-4">
                <span style="background-color: green; color: white; padding:5px; font-size:32px;">A LA UNE</span>
            </div>
            <div class="col-md-4">

            </div>
             <div class="col-md-4">
                <span style="margin-left: 100px;">Voir toutes les Actualités</span>
            </div>
        </div>
        <div class="row" >
            <div class="col-md-6">
                <div class="card" style="width: 580px;">
                    <video controls width="580px">
                        <source src="videos/PROJET-PODIA.mp4" type="video/mp4">

                        Sorry, your browser doesn't support embedded videos.
                    </video>
                    {{-- <img src="..." class="card-img-top" alt="..."> --}}
                    <div class="card-body">
                        <span style="font-weight: 400; color:dimgrey">
                            01/01/22
                        </span>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      <a href="#" class="py-2 d-block" style="color: green;">Voir Plus</a>
                    </div>
                  </div>
            </div>
            <div class="col-md-6 row">
                <div class="col-md-6">
                    <div class="card" style="width: 270px; margin:5px;">

                        <img src="images/Image-2-site-FK.jpeg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <span style="font-weight: 400; color:dimgrey">
                                01/01/22
                            </span>
                          <p class="card-text">Some quick example </p>
                          <a href="#" class="py-2 d-block" style="color: green;">Voir Plus</a>
                        </div>
                      </div>
                </div>
                <div class="col-md-6">
                    <div class="card" style="width: 270px; margin:5px;">

                        <img src="images/Image-3-[site-FK].jpeg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <span style="font-weight: 400; color:dimgrey">
                                01/01/22
                            </span>
                          <p class="card-text">Some quick example </p>
                          <a href="#" class="py-2 d-block" style="color: green;">Voir Plus</a>
                        </div>
                      </div>
                </div>
                <div class="col-md-6">
                    <div class="card" style="width: 270px; margin:5px;">

                        <img src="images/Image_4_site_FK].jpeg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <span style="font-weight: 400; color:dimgrey">
                                01/01/22
                            </span>
                          <p class="card-text">Some quick example </p>
                          <a href="#" class="py-2 d-block" style="color: green;">Voir Plus</a>
                        </div>
                      </div>
                </div>
                <div class="col-md-6">
                    <div class="card" style="width: 270px;< margin:5px;">

                        <img src="images/Image-5-[site-FK].jpeg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <span style="font-weight: 400; color:dimgrey">
                                01/01/22
                            </span>
                          <p class="card-text">Some quick example </p>
                          <a href="#" class="py-2 d-block" style="color: green;">Voir Plus</a>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section py-5 py-md-0 bg-light" style="margin-top: 50px;">
    <div class="container">
        <div class="col-md-4" style="margin-bottom: 25px;">
            <span style="background-color: green; color: white; padding:5px;">NOS PROGRAMMES POUR 2022
            </span>
        </div>
      <div class="row">
          <div class="col-md-4" style="background-color:orange;">
              <div style="margin-top: 30px; color:white ">
                <span >
                    AUTOMATISATION ECONOMIQUE DE LA FEMME
                </span>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.</p>
                <p><a href="/actualites" class="btn btn-secondary py-2 px-3">Voir plus</a></p>
              </div>

          </div>
          <div class="col-md-8" style="padding: 0;">
            <img src="images/Image6siteFK.jpeg" width="790px" style="padding: 0;" height="200px" alt="">
          </div>
      </div>
      <div class="row">

        <div class="col-md-8" style="padding: 0;">
            <img src="images/Image-8-[site-FK].jpeg" width="790px" style="padding: 0;" height="200px" alt="">
        </div>
        <div class="col-md-4" style="background-color:orange;">
            <div style="margin-top: 30px; color:white ">
              <span >
                AIDE AUX FAMILLES VULNERABLES
              </span>
              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.</p>
              <p><a href="/actualites" class="btn btn-secondary py-2 px-3">Voir plus</a></p>
            </div>

        </div>
      </div>
      <div class="row">
        <div class="col-md-4" style="background-color:orange;">
            <div style="margin-top: 30px; color:white ">
              <span >
                FORMATION
              </span>
              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.</p>
              <p><a href="/actualites" class="btn btn-secondary py-2 px-3">Voir plus</a></p>
            </div>

        </div>
        <div class="col-md-8" style="padding: 0;">
            <img src="images/Image-7-[site-FK].jpeg" width="790px" style="padding: 0;" height="200px" alt="">
        </div>
      </div>
      <div class="row">

        <div class="col-md-8" style="padding: 0;">
            <img src="images/Image-9-[site-FK].jpeg" width="790px" style="padding: 0;" height="200px" alt="">
        </div>
        <div class="col-md-4" style="background-color:orange;">
            <div style="margin-top: 30px; color:white ">
              <span >
                SCOLARISATION DE LA PETITE FILLE
              </span>
              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.</p>
              <p><a href="/actualites" class="btn btn-secondary py-2 px-3">Voir plus</a></p>
            </div>

        </div>
      </div>
    </div>
</section>

@foreach ($Une as $Une)
<section class="ftco-section ftco-causes" id="actualite" style="padding-top: 40px;">
    <div class="container">
        <div class="row justify-content-center pb-3">
            <div class="col-md-10 heading-section text-center ftco-animate">
                <h2 class="mb-4" style="color: darkgreen;">{{ $Une->title }}</h2>

            </div>
        </div>
    </div>

</section>
@endforeach


<section class="" style="padding-top:10px ;">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4" style="margin-bottom: 20px; text-align: center;">
                <span style="background-color: green; color: white; padding:5px; ">CHIFFRES CLES</span>
            </div>
            <div class="col-md-4"></div>

        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="rounded" style="background-color: skyblue; margin:2px;">
                    <div style="text-align: center; padding-top: 50px; padding-bottom: 50px">
                        <h3>5000</h3>
                        <p align="center" style="font-size: 24px; color:white;">Sac de riz distribué</p>
                    </div>

                </div>

            </div>
            <div class="col-md-4">
                <div class="rounded" style="background-color: skyblue; margin:2px;">
                    <div style="text-align: center; padding-top: 50px; padding-bottom: 50px">
                        <h3>8</h3>
                        <p align="center" style="font-size: 24px; color:white;">PARTENAIRES</p>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="rounded" style="background-color: skyblue; margin:2px;">
                    <div style="text-align: center; padding-top: 50px; padding-bottom: 50px">
                        <h3>5</h3>
                        <p align="center" style="font-size: 24px; color:white;">ANNEES D’EXISTENCE</p>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>
<section>
    <div class="container"  style="margin-top: 100px;">
        <div class="col-md-4">
            <span style="background-color: green; color: white; padding:5px;">NOS PARTENAIRES</span>
        </div>

        <div class="row" style="margin-top: 50px">
            <div class="col-sm-3">
                <div>
                    <img src="images/logo-exemple.png" alt="">
                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <img src="images/logo-exemple.png" alt="">

                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <img src="images/logo-exemple.png" alt="">

                </div>
            </div>
            <div class="col-sm-3">
                <div>
                    <img src="images/logo-exemple.png" alt="">

                </div>
            </div>
        </div>

    </div>
</section>
@endsection
