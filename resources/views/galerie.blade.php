@extends('layouts.layout-page')

@section('containt')

<section style="padding-top:100px; background-color:#f1efec;">
    <main>

        <section class="py-5 text-center container">
          <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
              <h1 class="fw-light">Album</h1>
              <p class="lead text-muted">La Fondation Kouyo en image</p>
              {{-- <p>
                <a href="#" class="btn btn-primary my-2">Main call to action</a>
                <a href="#" class="btn btn-secondary my-2">Secondary action</a>
              </p> --}}
            </div>
          </div>
        </section>
      
        <div class="album py-5 bg-light">
          <div class="container">
      
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                @foreach ($galeries as $galerie)
                <div class="col">
                    <div class="card shadow-sm">
                      <img src="/storage/{{ $galerie->image }}" style="object-fit: cover; width: 330px; height: 250px;" alt="{{ $galerie->titre }}">
          
                      <div class="card-body">
                        <p class="card-text">{{ $galerie->descriptif }}</p>
                        <div class="d-flex justify-content-between align-items-center">
                          {{-- <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                            <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                          </div> --}}
                          <small class="text-muted">9 mins</small>
                        </div>
                      </div>
                    </div>
                  </div>   
                @endforeach
             
            
      
            </div>
            <div class="row mt-5">
                <div class="col">
                    <div class="block-27">
                        
                        <ul>
                            {{ $galeries->links() }}
                            
                        </ul>
                    </div>
                </div>
            </div>
          </div>
          
        </div>
      
      </main>
</section>

@endsection