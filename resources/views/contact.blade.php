@extends('layouts.layout-fix')

@section('containt')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/36757734_m.jpg');" data-stellar-background-ratio="0.5" >
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center" style="padding-bottom: 100px;">
        <div class="col-md-9 ftco-animate pb-5 text-center">
          <h2 class="mb-3 bread">Contactez-Nous</h2>
          <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Accueil <i class="ion-ios-arrow-forward"></i></a></span> <span>Contact <i class="ion-ios-arrow-forward"></i></span></p>
        </div>
      </div>
    </div>
  </section>
      
      <section class="contact-section bg-primary">
    <div class="container">
      <div class="row no-gutters d-flex contact-info">
        <div class="col-md-3 d-flex">
            <div class="align-self-stretch box p-4 py-md-5 text-center">
                <div class="icon d-flex align-items-center justify-content-center">
                    <span class="icon-map-signs"></span>
                </div>
                <h3 class="mb-4">Adresse</h3>
              <p>Cocody 2 Plateaux</p>
            </div>
        </div>
        <div class="col-md-3 d-flex">
            <div class="align-self-stretch box p-4 py-md-5 text-center">
                <div class="icon d-flex align-items-center justify-content-center">
                    <span class="icon-phone2"></span>
                </div>
                <h3 class="mb-4"> Téléphone </h3>
              <p><a href="tel://+22548991068">+225 48 99 10 68 <br> +225 22 46 34 26</a></p>
            </div>
        </div>
        <div class="col-md-3 d-flex">
            <div class="align-self-stretch box p-4 py-md-5 text-center">
                <div class="icon d-flex align-items-center justify-content-center">
                    <span class="icon-paper-plane"></span>
                </div>
                <h3 class="mb-4">Email </h3>
              <p><a href="mailto:info@yoursite.com">info@fondationkouyo.org</a></p>
            </div>
        </div>
        <div class="col-md-3 d-flex">
            <div class="align-self-stretch box p-4 py-md-5 text-center">
                <div class="icon d-flex align-items-center justify-content-center">
                    <span class="icon-globe"></span>
                </div>
                <h3 class="mb-4">Website</h3>
              <p><a href="#">fondationkouyo.org</a></p>
            </div>
        </div>
      </div>
    </div>
  </section>
          
      <section class="ftco-section ftco-no-pt ftco-no-pb contact-section">
    <div class="container-fluid px-0">
      <div class="row no-gutters block-9">
        <div class="col-md-6 order-md-last d-flex">
          <form action="#" class="bg-light p-5 contact-form">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Votre Nom">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Object">
            </div>
            <div class="form-group">
              <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
            </div>
            <div class="form-group">
              <input type="submit" value="Envoyer" class="btn btn-primary py-3 px-5">
            </div>
          </form>
        
        </div>

        <div class="col-md-6 d-flex">
            <div id="" class="bg-white mapy"><iframe class="mapy" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.18104642968!2d-3.9876089855525643!3d5.389356536802601!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc193eb788fa8b3%3A0x341595525f13292e!2sCOOK%20AFRICA!5e0!3m2!1sfr!2sci!4v1608918029926!5m2!1sfr!2sci" width="700" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
        </div>
      </div>
    </div>
  </section>

@endsection