

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">

      <div class="container">
      <a class="navbar-brand" href="/"> <img src="images/logo_kouyo fondation.png" alt="" width="100px"> </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">

        <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a href="/" class="nav-link">Accueil</a>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Qui Sommes-nous
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="/mot-president">Mot du Président</a>
                <a class="dropdown-item" href="/presentation">Présentation</a>
                <a class="dropdown-item" href="/equipe">Nos Equipes</a>

            </div>
            </li>
            <li class="nav-item"> <a class="nav-link" href="">Nos Actions</a> </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Notre Actualités
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/articles">Articles</a>
              <a class="dropdown-item" href="/video">Videos</a>
            </div>
          </li>
          <li class="nav-item">
               <a class="nav-link" href="">Notre Galerie</a>
          </li>

          <li class="nav-item"><a href="/contact" class="nav-link">Contactez-Nous</a></li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->

            @guest

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <ul>
                        <li class="nav-item">
                            <div style="align-content: right; padding: 0px">
                                <a href="/actualites" class="btn btn-danger">Faire un don</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{-- {{ __('Logout') }} --}}
                                    Déconnexion
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>

                </li>
            @endguest
        </ul>
      </div>
    </div>
</nav>
