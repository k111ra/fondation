@extends('layouts.layout-page')

@section('containt')

<section style="padding-top:100px; background-color:#f1efec;">
    <div class="container">

        <div class="row">
            <aside class="col-md-4 blog-sidebar">

                <div class="sidebar-box ftco-animate fadeInUp ftco-animated">
                    <h3 class="heading-2">Information Générale</h3>
                    <ul class="categories">
                        <li class="active"><a href="/mot-president">Mot du Président </a></li>
                        <li><a href="/presentation">Présentation</a></li>
                        <li><a href="/equipe">Nos Equipes </a></li>
                    </ul>
                </div>

                {{-- <div class="p-4">
                    <h4 class="">Ressource et Media</h4>
                    <ol class="list-unstyled">
                        <li><a href="#">Lettre Annuel</a></li>
                        <li><a href="#">Rapport Annuel </a></li>
                        <li><a href="#">Presse</a></li>
                    </ol>
                </div> --}}
            </aside><!-- /.blog-sidebar -->
           


            <div class="col-md-8 blog-main">
                <h2
                    style="font-family: Georgia,serif; font-size:13px; line-height:15px; font-style: italic; color: rgb(16 165 35); text-align: center;">
                    <span class="line"
                        style="width: 30px; height:1px; background-color:black; display:inline-block; margin: 0 5px 3px 5px"></span>
                    Qui Sommes Nous
                    <span class="line"
                        style="width: 30px; height:1px; background-color:black; display:inline-block; margin: 0 5px 3px 5px"></span>
                </h2>
                <h3 class="pb-4 mb-4 ">
                    Mot du Président-Fondateur de la Fondation KOUYO
                    <hr style="border-top: 3px solid #bbb;">


                </h3>
                @foreach ($motPresidents as $motPresident)
                <div class="blog-post">

                    {!! $motPresident->body !!}


                </div><!-- /.blog-post -->
                @endforeach
                {{-- <div class="container">
                    <div class="row">
                        <div class="col-sm">
                            <img src="images/Conference-Fondation-Kouyo-0009.jpg" width="100%" alt="">
                        </div>
                        <div class="col-sm">
                            <p>

                                Bonjour chers internautes. Akwaba sur le site
                                <br>
                                internet de la Fondation Kouyo.

                                La Fondation Kouyo que vous visitez en ce jour est une institution non gouvernementale
                                qui œuvre pour : <br>

                                • La stabilité de la cellule familiale,<br>

                                • La promotion l'éducation, l'alphabétisation et la scolarisation de la petite fille
                                <br>

                                • La Lutte contre toute formes de violence conjugale et de viole, <br>

                                • La Lutte contre les mutilations génitales féminines,<br>

                                • La Lutte contre le divorces, les séparations et les abandons de foyer,<br>

                                Ce n'est qu'ensemble que nous pourrons y arriver. N'hésitez donc pas à nous contacter et
                                à participer ainsi à cette noble tâche.
                            </p>
                        </div>

                    </div>
                </div> --}}



            </div><!-- /.blog-main -->



        </div>

    </div>
</section>

@endsection