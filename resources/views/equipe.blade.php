@extends('layouts.layout-page')

@section('containt')
<section style="padding-top:100px; background-color:#f1efec;">
    <div class="container">

        <div class="row">
            <aside class="col-md-4 blog-sidebar">

                <div class="sidebar-box ftco-animate fadeInUp ftco-animated">
                    <h3 class="heading-2">Information Générale</h3>
                    <ul class="categories">
                        <li ><a href="/mot-president">Mot du Président </a></li>
                        <li ><a href="/presentation">Présentation</a></li>
                        <li class="active"><a href="/nos-equipes">Nos Equipes </a></li>                        
                    </ul>
                </div>

                {{-- <div class="p-4">
                    <h4 class="">Ressource et Media</h4>
                    <ol class="list-unstyled">
                        <li><a href="#">Lettre Annuel</a></li>
                        <li><a href="#">Rapport Annuel </a></li>
                        <li><a href="#">Presse</a></li>
                    </ol>
                </div> --}}
            </aside><!-- /.blog-sidebar -->
            <style>
                /* Style the buttons */
                .btn {
                    border: none;
                    outline: none;
                    padding: 10px 16px;
                    background-color: #f1f1f1;
                    cursor: pointer;
                }

                /* Style the active class (and buttons on mouse-over) */
                .active,
                .btn:hover {
                    background-color: #10a523;
                    color: white;
                }
            </style>

            @foreach ($equipePage as $equipePage)
            <div class="col-md-8 blog-main">
                <h2 style="font-family: Georgia,serif; font-size:13px; line-height:15px; font-style: italic; color:#10a523; text-align: center;">
                    <span class="line" style="width: 30px; height:1px; background-color:black; display:inline-block; margin: 0 5px 3px 5px"></span>
                    {{ $equipePage->title }}
                    <span class="line" style="width: 30px; height:1px; background-color:black; display:inline-block; margin: 0 5px 3px 5px"></span>
                </h2>
                <h3 class="pb-4 mb-4 ">
                    {{ $equipePage->excerpt }}
                    <hr style="border-top: 3px solid #bbb;">
                </h3>

                <div class="blog-post">

                    <div class="w3-container">
                        <div class="row" style="padding: 10px;">
                            @foreach ($equipes as $equipe)
                            <div style="padding: 10px;">
                                <div class="w3-card-4" style="width:230px; background-color:white; padding:10px;">
                                    <img src="storage/{{ $equipe->image }}" alt="president-fondateur fondation Kouyo" style="width:100%">
                                    <div class="w3-container w3-center">
                                      <p> <h3>{{ $equipe->nom }}</h3> <br>{{ $equipe->titre }} <br>{{ $equipe->email }}</p>
                                      
                                    </div>
                                  </div>
                            </div>
                            
                            @endforeach
                        </div>
                        
                      
                      </div>


                </div><!-- /.blog-post -->


            </div><!-- /.blog-main -->
            @endforeach

            



        </div>

    </div>
</section>
@endsection