<?php

namespace App\Http\Controllers;

use App\Models\Galerie;
use Illuminate\Http\Request;
use App\Models\Post;


class GallerieController extends Controller
{
    public function showDetail() {
        
        $galerie = Galerie::orderBy('id','DESC')->paginate(6);
        $penseeFooter = Post::where('slug','pensee-du-jour')->get();
        

        return view('galerie')->with([
            
            'penseeFooter' => $penseeFooter,
            'galeries' => $galerie,
            
            ]);
    }
}
