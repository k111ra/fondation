<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Post;
use Illuminate\Http\Request;

class MotPresidentController extends Controller
{
    public function showDetail() {
        $categoryMotPresident = Categorie::Where('slug', '=', 'mots-du-president')->first();
        $motPresidents = Post::where('category_id', '=', $categoryMotPresident->id)->paginate(1);
        $penseeFooter = Post::where('slug','pensee-du-jour')->get(); 
        
        // return dd($MotPresident);

        return view('mot-president')->with([
            'motPresidents' => $motPresidents,
            'penseeFooter' => $penseeFooter,
            
            ]);
    }
}
