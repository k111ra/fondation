<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Post;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Page;

class PresentationController extends Controller
{
    public function showDetail() {
        $pagePresentation = Page::where('slug','presentation')->get();
        $categoryPresentation = Categorie::Where('slug', '=', 'presentation')->first();
        $presentations = Post::where('category_id', '=', $categoryPresentation->id)->paginate(4);
        $penseeFooter = Post::where('slug','pensee-du-jour')->get(); 
        
        // return dd($MotPresident);

        return view('presentation')->with([
            'presentations' => $presentations,
            'penseeFooter' => $penseeFooter,
            'pagePresentation' => $pagePresentation,
            
            ]);
    }
}
