<?php

namespace App\Http\Controllers;

use App\Models\Equipe;
use App\Models\Post;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Page;

class EquipeController extends Controller
{
    public function showDetail() {
        $equipePage = Page::where('slug','equipe')->get();
        $equipes = Equipe::orderBy('id')->get();
        $penseeFooter = Post::where('slug','pensee-du-jour')->get();
        

        return view('equipe')->with([
            'equipes' => $equipes,
            'equipePage' => $equipePage,
            'penseeFooter' => $penseeFooter,
            
            ]);
    }
}
