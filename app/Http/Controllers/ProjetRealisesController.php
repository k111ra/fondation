<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\Page;

class ProjetRealisesController extends Controller
{
    public function showDetail() {
        $pageProjetRealises = Page::where('slug','projets-realises')->get();
        $categoryProjetRealises = Category::Where('slug', '=', 'projets-realises')->first();
        $projetRealises = Post::where('category_id', '=', $categoryProjetRealises->id)->paginate(4);
        $penseeFooter = Post::where('slug','pensee-du-jour')->get(); 
        
        // return dd($projetRealises);

        return view('projet_realises')->with([
            'projetRealises' => $projetRealises,
            'penseeFooter' => $penseeFooter,
            'pageProjetRealises' => $pageProjetRealises,
            
            ]);
    }
}
