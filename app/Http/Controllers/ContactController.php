<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function showDetail() {
        
        $penseeFooter = Post::where('slug','pensee-du-jour')->get();
        

        return view('contact')->with([
            
            'penseeFooter' => $penseeFooter,
            
            ]);
    }
}
