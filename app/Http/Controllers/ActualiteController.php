<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Post;
use Illuminate\Http\Request;

class ActualiteController extends Controller
{
    public function showDetail() {
        $categoryArticle = Categorie::Where('slug', '=', 'articles')->first();
        $articles = Post::where('category_id', '=', $categoryArticle->id)->paginate(3);
        $penseeFooter = Post::where('slug','pensee-du-jour')->get();


        return view('actualites')->with([
            'articles'=>$articles,
            'penseeFooter' => $penseeFooter,

            ]);
    }

    public function article(){
        $categoryArticle = Categorie::Where('slug', '=', 'articles')->first();
        $articles = Post::where('category_id', '=', $categoryArticle->id)->paginate(3);
        $penseeFooter = Post::where('slug','pensee-du-jour')->get();


        return view('articles')->with([
            'articles'=>$articles,
            'penseeFooter' => $penseeFooter,

            ]);
    }
    public function video(){
        $penseeFooter = Post::where('slug','pensee-du-jour')->get();
        return view('video')->with([
            'penseeFooter' => $penseeFooter,
        ]);
    }
}
