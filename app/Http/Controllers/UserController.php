<?php

namespace App\Http\Controllers;

// use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use TCG\Voyager\Models\User;

class UserController extends Controller
{
    //
    public function store(request $request ){
        Validator::make($request->all(), [
		    'name' => 'required|max:100',
            'phone' => 'required',
		    'email' => 'required|email|unique:users,email',
		    'password' => 'required|min:5',
		    // 'role' => 'required',
		], [
			'required' => 'Le champ :attribute est obligatoire.',
			'unique' => 'La valeur du champ :attribute a déjà été utilsée.',
			'email' => 'L\' :attribute n\'est pas correct.',
			'min' => 'Le champ :attribute doit contenir au minimum :min caractères.',
			'max' => 'Le champ :attribute doit contenir au minimum :max caractères.',
		])->validate();

    	$user = new User();
    	$user->name = $request->name;
    	$user->phone = $request->phone;
    	$user->email = $request->email;
    	$user->password = Hash::make($request->password);
    	$user->remember_token = '';
    	// $user->role = $request->role;
        $user->save();

        // $role = Role::find($request->input('role_id'));

        // $user->attachRole($role);

    	$request->session()->flash('status', 'Un nouvel utilisateur a été ajouté');
    	return view('/index');
    }
}
