<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Post;
use App\Models\Slide;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function test()
    {
        $slides = Slide::orderBy('id','DESC')->paginate(3);

        $categoryArticle = Categorie::Where('slug', '=', 'articles')->first();
        $articles = Post::where('category_id', '=', $categoryArticle->id)->paginate(3);

        $categoryMotPresident = Categorie::Where('slug', '=', 'mots-du-president')->first();
        $motPresidents = Post::where('category_id', '=', $categoryMotPresident->id)->paginate(1);

        $presentation1 = Post::where('slug','presentation-1')->get();
        $presentation2 = Post::where('slug','presentation-2')->get();
        $presentation3 = Post::where('slug','presentation-3')->get();
        $presentation4 = Post::where('slug','presentation-4')->get();

        $categoryUne = Categorie::Where('slug', '=', 'a-la-une')->first();
        $Une = Post::where('category_id', '=', $categoryUne->id)->paginate(1);

        $categoryPropos = Categorie::Where('slug', '=', 'propos-recueillis')->first();
        $propos = Post::where('category_id', '=', $categoryPropos->id)->paginate(1);

        $categoryPensee = Categorie::Where('slug', '=', 'pensee-du-jour')->first();
        $pensee = Post::where('category_id', '=', $categoryPensee->id)->paginate(1);
        $penseeFooter = Post::where('category_id', '=', $categoryPensee->id)->paginate(1);



        return view('index')->with([
            'articles'=>$articles,
            'slides'=>$slides,
            'motPresidents'=>$motPresidents,
            'presentation1'=>$presentation1,
            'presentation2'=>$presentation2,
            'presentation3'=>$presentation3,
            'presentation4'=>$presentation4,
            'Une'=>$Une,
            'propos'=>$propos,
            'pensee'=>$pensee,
            'penseeFooter'=>$penseeFooter,


        ]);
        return view('home');
    }
    public function index()
    {

        return view('welcome');
    }
}
