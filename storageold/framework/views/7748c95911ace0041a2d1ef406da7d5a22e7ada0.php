<?php $__env->startSection('containt'); ?>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.js"></script>

<style type="text/css">
    #overlay {
        position: relative;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: #000;
        filter: alpha(opacity=70);
        -moz-opacity: 0.7;
        -khtml-opacity: 0.7;
        opacity: 0.7;
        z-index: 1000000000;
        display: none;
    }

    .cnt223 a {
        text-decoration: none;
    }

    .popup {
        width: 100%;
        margin: 0 auto;
        display: none;
        position: relative;
        z-index: 1000000001;
    }

    .cnt223 {

        min-width: 200px;
        width: 400px;
        min-height: 150px;
        margin-top: 500px;
        background: green;
        position: fixed;
        z-index: 103;
        padding: 15px 35px;
        border-radius: 5px;
        box-shadow: 0 2px 5px #000;
    }

    .cnt223 p {
        clear: both;
        color: #555555;
        /* text-align: justify; */
        font-size: 14px;
        font-family: sans-serif;
    }

    .cnt223 p a {
        color: whitesmoke;
        font-weight: bold;
    }

    .cnt223 .x {
        float: right;
        height: 35px;
        left: 22px;
        position: fixed;
        top: -25px;
        width: 34px;
    }

    .cnt223 .x:hover {
        cursor: pointer;
    }
</style>
<script type='text/javascript'>
    $(function () {
        var overlay = $('<div id="overlay"></div>');
        overlay.show();
        overlay.appendTo(document.body);
        $('.popup').show();
        $('.close').click(function () {
            $('.popup').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });




        $('.x').click(function () {
            $('.popup').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });
    });
</script>
<?php $__currentLoopData = $pensee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pensee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class='popup'>
    <div class='cnt223'>
        <a href='' class='close'>X</a>
        <h5 style="color:honeydew;">Pensée du jour:</h5>
        <p style="color: honeydew;">

            <?php echo $pensee->body; ?>


        </p>
    </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<section class="">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">

            <?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="carousel-item <?php echo e($slide->active); ?>">
                <img class="slide d-block w-100 slider " src="/storage/<?php echo e($slide->image); ?>" alt="<?php echo e($slide->titre); ?>">

                <div class="carousel-caption  d-md-block">
                    <div class="row">



                        <p style="display: inline-block; transform: rotate(-90deg); ">
                            <a onclick="document.getElementById('id01').style.display='block'"
                                class="w3-button btn-primary" style="background-color:orange;"><i
                                    class="ion-ios-play "></i></span> <span class="play"
                                    style="color: green; ">Video</span></a>
                        </p>

                        <div class="col text-right ">
                            <h1 class=" font-weight-bold slideTest" style="color: green;" > <?php echo e($slide->text); ?></h1>
                            <p class="btn btn-primary py-3 px-4 slideTest" ><a href="/contact" >Rejoignez-nous
                                </a>
                            </p>
                        </div>

                    </div>


                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


        </div>

        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>


        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>






<div id="id01" class="w3-modal" style="z-index: 100000">
    <div class="w3-modal-content">
        <div class="w3-container">
            
                <span onclick="document.getElementById('id01').style.display='none'"
                class="w3-button w3-display-topright">&times;</span>
                <div class="col-md-12 d-flex ftco-animate">
                    <div class="blog-entry align-self-stretch d-md-flex">
                        <video width="320" height="240" controls="true">
                            <source src="videos/PROJET-PODIA.mp4" type="video/mp4">
                            
                          Your browser does not support the video tag.
                          </video>
                        <div class="text d-block pl-md-4">
                            <div class="meta mb-3">
                                <div><a href="/video"></a></div>
                            </div>
                            <h3 class="heading"><a href="/video">PROJET PÔLE D'INTÉGRATION AGROPASTORALE (PODIA) AVEC LE RAFAA.</a></h3>
                            <p>27 janvier 2022 - Visite du site à Sikensi pour la mise en œuvre du PODIA (Acte 1) en partenariat avec le Ministère de la Femme, de la Famille et de l'Enfant pour le compte de la femme avec le RAFAA.</p>
                            <p><a href="/video" class="btn btn-primary py-2 px-3">Voir plus</a></p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<section class="ftco-section ftco-no-pt ftco-no-pb ftco-volunteer">
    <div class="container">
        <div class="row">
            <div class="col-md-7 img-volunteer"
                style="background-image: url(images/event/Anderson_kouyo.png); background-size: 100%; background-repeat: no-repeat;">
                <div class="row no-gutters justify-content-end">
                    <div class="col-md-4">
                        <?php $__currentLoopData = $motPresidents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $motPresident): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="padding-mot" style=""></div>
                        <div class="text">
                            <h2 class="mb-4"><?php echo e($motPresident->title); ?></h2>
                            <p style="color:green;"><?php echo e($motPresident->excerpt); ?></p>
                            <p class="text-center"><a href="/mot-president" class="btn btn-primary py-3 px-4">Voir
                                    Plus</a></p>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>
                </div>
            </div>
            <div class="col-md-5 d-flex align-items-center ">
                <div>
                    <div id="actualite" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#actualite" data-slide-to="0" class="active"></li>
                            <li data-target="#actualite" data-slide-to="1"></li>
                            <li data-target="#actualite" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="carousel-item <?php echo e($article->active); ?>">
                                <img class="d-block w-100 actu" src="storage/<?php echo e($article->image); ?>"
                                    alt="<?php echo e($article->title); ?>">
                               <a href="/actualites">
                                <div class="carousel-caption  d-md-block slideTes">
                                    <h3 class="font-weight-bold" ><?php echo e($article->title); ?></h3>
                                    <p ><?php echo e($article->excerpt); ?></p>
                                </div>
                                </a>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                        </div>
                        <a class="carousel-control-prev" href="#actualite" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#actualite" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>



            </div>
        </div>
    </div>
</section>

<section class="services-section py-5 py-md-0 bg-light">
    <div class="container">
        <div class="row no-gutters d-flex">
            <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block">
                    <div class="icon"><span class="flaticon-charity"></span></div>
                    <?php $__currentLoopData = $presentation1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $presentation1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="media-body">
                        <h3 class="heading mb-3"><?php echo e($presentation1->title); ?></h3>
                        <p><?php echo e($presentation1->excerpt); ?></p>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block">
                    <div class="icon"><span class="flaticon-adoption"></span></div>
                    <?php $__currentLoopData = $presentation2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $presentation2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="media-body">
                        <h3 class="heading mb-3"><?php echo e($presentation2->title); ?></h3>
                        <p><?php echo e($presentation2->excerpt); ?></p>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block">
                    <div class="icon"><span class="flaticon-volunteer"></span></div>
                    <?php $__currentLoopData = $presentation3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $presentation3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="media-body">
                        <h3 class="heading mb-3"><?php echo e($presentation3->title); ?></h3>
                        <p><?php echo e($presentation3->excerpt); ?></p>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block">
                    <div class="icon"><span class="flaticon-open-book"></span></div>
                    <?php $__currentLoopData = $presentation4; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $presentation4): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="media-body">
                        <h3 class="heading mb-3"><?php echo e($presentation4->title); ?></h3>
                        <p><?php echo e($presentation4->excerpt); ?></p>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $__currentLoopData = $Une; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Une): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<section class="ftco-section ftco-causes" id="actualite" style="padding-top: 40px;">
    <div class="container">
        <div class="row justify-content-center pb-3">
            <div class="col-md-10 heading-section text-center ftco-animate">
                <h2 class="mb-4" style="color: darkgreen;"><?php echo e($Une->title); ?></h2>

            </div>
        </div>
    </div>
    <div class="container" style="padding-top: 40px;">
        <div class="row">
            <div class="col-4 ">
                <img src="images/23633[1].jpg" alt="" width="350px">
            </div>
            <div class="col-sm-8" style="padding-left: 40px;">
                <H3><?php echo e($Une->excerpt); ?></H3>
                <p style="color: teal;"><?php echo $Une->body; ?></p>
            </div>
        </div>
    </div>
</section>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<section class="testimony-section" style="padding-top:10px ;">
    <div class="container">
        <div class="row ftco-animate justify-content-center">

            <div class="col-md-12 ">

                <div class="heading-section heading-section-white pt-4 ftco-animate">
                    <h2 class="mb-0">Propos recueillis</h2>
                </div>
                
                    <?php $__currentLoopData = $propos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $propo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item">

                        <div class="testimony-wrap pb-4">
                            <div class=" row col-md media">
                                <div class="col-md-4">
                                    <img class="rounded-circle " src="storage/<?php echo e($propo->image); ?>" alt="" width="100%">
                                </div>
                                <div class="col-sm">
                                    <?php echo $propo->body; ?>

                                </div>

                            </div>

                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                    <!-- <div class="item">
                    <div class="testimony-wrap pb-4">
                        <div class="text">
                            <p class="mb-4">"J'ai une admiration pour les domaines d’intervention de la Fondation
                                Kouyo. Je suis sensiblement intéressé par la Protection des enfants et les activités
                                Génératrices de Revenues en faveur des femmes et des Jeunes"</p>
                        </div>
                        <div class="d-flex">
                            <div class="user-img" style="background-image: url()">
                            </div>
                            <div class="pos ml-3">
                                <p class="name">Jean Baptiste OUEDRAOGO</p>
                                <span class="position">Directeur du Département de la Coopération au Développement
                                    du Conseil de l'Entente.</span>
                            </div>
                        </div>
                    </div>
                </div> -->


                
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\fondation\resources\views/index.blade.php ENDPATH**/ ?>