<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');
Route::get('/test', 'HomeController@test');
Route::get('/register', 'HomeController@register')->name('register');
Route::post('/user_registration', 'UserController@store')->name('user_registration');

Route::get('/index', 'HomeController@showBlogList')->name('home');


Route::get('/mot-president','MotPresidentController@showDetail');

Route::get('/presentation','PresentationController@showDetail');
Route::get('/projet_realises','ProjetRealisesController@showDetail');

Route::get('/equipe','EquipeController@showDetail');

Route::get('/contact','ContactController@showDetail');

Route::get('/actualites','ActualiteController@showDetail');
Route::get('/video','ActualiteController@video');
Route::get('/articles','ActualiteController@article');



Route::get('/galerie','GallerieController@showDetail');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
